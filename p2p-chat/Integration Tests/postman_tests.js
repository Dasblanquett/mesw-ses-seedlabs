//Access Level Tests

pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});

pm.test("Body matches string", function () {
    pm.expect(pm.response.text()).to.include("Unauthorized");
});


//Token Tests

pm.test("Status code is 401", function () {
    pm.response.to.have.status(401);
});

pm.test("Body matches string", function () {
    pm.expect(pm.response.text()).to.include("No authorization token");
});