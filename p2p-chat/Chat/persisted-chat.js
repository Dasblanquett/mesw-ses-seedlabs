// code adapted from: https://www.elastic.co/pt/videos/how-to-write-a-p2p-chat-app-in-nodejs-by-mathias-buus
const https = require('http');

require('lookup-multicast-dns/global')
var topology = require('fully-connected-topology')
var register = require('register-multicast-dns')
require("dotenv").config();
var toPort = require('hash-to-port')
var level = require('level')
var jwt = require("jsonwebtoken");
const secret = process.env.JWT_SECRET;
var scuttleup = require('scuttleup') // levelup (LevelDB) override that syncs DB's between users
                                     // LevelDB is a simple key-value store built by Google.
                                     // Compresses data
var crypto = require('crypto');
var key = process.env.KEY;

const algorithm = 'aes-256-ctr'; // supported by OpenSSL
const ENCRYPTION_KEY = Buffer.from(process.env.KEY, 'base64');
const IV_LENGTH = 16;

var me = process.argv[2]
var peer = process.argv[3]
var accessToken = process.argv[4]

var db = level(me + '.db')
var username = me
var logs = scuttleup(db, {valueEncoding: 'json'})
var swarm = topology(toAddress(username), toAddress(peer))

register(me)

function encrypt(text) {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text) {
  let textParts = text.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

swarm.on('connection', function (socket, id) {
  console.log('info> direct connection to', id);

  socket.pipe(logs.createReplicationStream({live: true})).pipe(socket)
})

function verifyBadSignature(data) {
  const issuedDate = Date.now();

  const signature = data.entry.signature;
  const payloadDate = data.entry.timestamp;
  const payloadUsername = data.entry.username;
  const payloadMessage = data.entry.message;

  const decoded = jwt.verify(signature, secret);

  const isGoodData = decoded.username.toString() === payloadUsername.toString() &&
      decoded.data.toString() === payloadMessage.toString() &&
      decoded.date.toString() === payloadDate.toString();

  const replay = issuedDate - parseInt(payloadDate) >= 10000000;

  return replay || !isGoodData;
}

logs.createReadStream({live: true})
    .on('data', function (data) {

      if(!verifyBadSignature(data)){

        data.entry.message = decrypt(data.entry.message);

        console.log(`(${data.entry.timestamp})` + data.entry.username + '> ' + data.entry.message)
      }else{
        console.log("RESART YOUR CONNECTION");
      }
    })

function createSignedData(user, data, issuedDate) {

  let userData = {
    username: user,
    data: data,
    date: issuedDate,
  };

  //Prepare JWT token for authentication
  const jwtPayload = userData;
  const jwtData = {
    expiresIn: process.env.JWT_TIMEOUT_DURATION,
  };
  const secret = process.env.JWT_SECRET;
  //Generated JWT token with Payload and secret.
  return jwt.sign(jwtPayload, secret, jwtData);
}

process.stdin.on('data', function (data) {

  var encrypted = encrypt(data.toString().trim());

  const issuedDate = Date.now();
  const signature = createSignedData(me, encrypted, issuedDate);

  logs.append({username: me, timestamp: issuedDate, message: encrypted, signature: signature})
})

function toAddress (name) {
  return name + '.local:' + toPort(name)
}
