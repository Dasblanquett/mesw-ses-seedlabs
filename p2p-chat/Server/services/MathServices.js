exports.NRoot = function(index, parameter) {
	index = Math.abs(index);
	parameter = Math.abs(parameter);

	const result = Math.pow(parameter, 1/index);
	const root = {
		result: result,
		index: index,
		parameter: parameter,
	};

	return root;
};