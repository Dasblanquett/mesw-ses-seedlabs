const mailer = require( "../helpers/mailer");
const UserModel = require("../models/UserModel");
const jwt = require("jsonwebtoken");
const apiResponse = require("../helpers/apiResponse");
const bcrypt = require("bcrypt");
const utility = require("../helpers/utility");
const {constants} = require("../helpers/constants");

/**
 * Login User Service
 * @param req Request
 * @param res Response callback
 * @returns response json
 * @constructor
 */
exports.UserLogin = function(req, res) {
	let msgInfo = "Validation Error.";
	UserModel.findOne({email : req.body.email}).then(user => {
		if (user) {
			//Compare given password with db's hash.
			bcrypt.compare(req.body.password,user.password,function (err,same) {
				if(same){
					//Check account confirmation.
					if(user.isConfirmed){
						// Check User's account active or not.
						if(user.status) {
							let userData = {
								_id: user._id,
								firstName: user.firstName,
								lastName: user.lastName,
								twoFactor: false,
								email: user.email,
								accessLevel: user.accessLevel
							};
							//Prepare JWT token for authentication
							const jwtPayload = userData;
							const jwtData = {
								expiresIn: process.env.JWT_TIMEOUT_DURATION,
							};
							const secret = process.env.JWT_SECRET;
							//Generated JWT token with Payload and secret.
							userData.token = jwt.sign(jwtPayload, secret, jwtData);

							const otp = utility.generateRandomOTP(8);

							UserModel.findOneAndUpdate({email : req.body.email}, {
								confirmOTP: otp
							}).catch(err => {
								return apiResponse.ErrorResponse(res, err);
							}).then( ()=>{

								// Html email body
								let html = mailer.buildEmail(String(otp));
								// Send confirmation email
								mailer.send(
									constants.confirmEmails.from,
									req.body.email,
									"2FA: Confirm you login",
									html
								).then(function(){
									return apiResponse.successResponseWithData(res,"Login Success.", userData);
								}).catch(err => {
									console.log(err);
									return apiResponse.ErrorResponse(res,err);
								}) ;
							});

						}else {
							msgInfo = "ERROR: Account is not active.";
							return apiResponse.unauthorizedResponse(res, msgInfo);
						}
					}else{
						msgInfo = "ERROR: Account is not confirmed. Missing OTP Validation.";
						return apiResponse.unauthorizedResponse(res, msgInfo);
					}
				}else{
					msgInfo = "ERROR: Email or Password wrong.";
					return apiResponse.unauthorizedResponse(res, msgInfo);
				}
			});
		}else{
			msgInfo = "ERROR: Email or Password wrong.";
			return apiResponse.unauthorizedResponse(res, msgInfo);
		}
	});
};

/**
 * Validate if Email Exists
 *
 * @param value The email
 * @returns {UserDTO} Found user where email=value
 * @constructor
 */
exports.CheckIfEmailExists = function(value) {
	return UserModel.findOne({email : value}).then((user) => {
		if (user) {
			return Promise.reject("E-mail already in use");
		}
	});
};

/**
 * ConfirmOTP using email
 *
 * @param email The email
 * @param otp The OTP
 * @param res The Response
 * @returns {json} The Response
 * @constructor
 */
exports.ConfirmOTP = function(email, otp, password, res) {
	return UserModel.findOne({email : email}).then(user => {
		if (user) {
			//Check already confirm or not.
			if(!user.isConfirmed){
				//Check account confirmation.
				if(user.confirmOTP == otp){

					//hash input password
					// creates a password with: $[algorithm]$[cost]$[salt][hash]
					// default algorithm is The Blowfish Encryption Algorithm
					// https://www.schneier.com/academic/blowfish/
					bcrypt.hash(password,parseInt(process.env.SALT),function(err, hash) {

						//Update user as confirmed
						UserModel.findOneAndUpdate({email : email}, {
							isConfirmed: 1,
							confirmOTP: null,
							password: hash
						}).catch(err => {
							return apiResponse.ErrorResponse(res, err);
						});
						return apiResponse.successResponse(res,"Valid OTP: Account confirmed");
					});
				}else{
					return apiResponse.unauthorizedResponse(res, "Invalid OTP: Otp does not match");
				}
			}else{
				return apiResponse.unauthorizedResponse(res, "Invalid OTP: Account is already valid.");
			}
		}else{
			return apiResponse.unauthorizedResponse(res, "Invalid OTP: Specified email not found. Register an account using this e-mail first.");
		}
	});
};

/**
 * ConfirmOTP using email
 *
 * @param email The email
 * @param otp The OTP
 * @param res The Response
 * @returns {json} The Response
 * @constructor
 */
exports.VerifyTwoFactor = function(email, otp, res) {
	return UserModel.findOne({email : email}).then(user => {
		if (user) {
			//Check already confirm or not.
			if(user.isConfirmed){
				//Check account confirmation.
				if(user.confirmOTP == otp){
					//Update user as confirmed
					UserModel.findOneAndUpdate({email : email}, {
						confirmOTP: null
					}).catch(err => {
						return apiResponse.ErrorResponse(res, err);
					});


					let userData = {
						_id: user._id,
						firstName: user.firstName,
						lastName: user.lastName,
						twoFactor: true,
						email: user.email,
						accessLevel: user.accessLevel
					};
					//Prepare JWT token for authentication
					const jwtPayload = userData;
					const jwtData = {
						expiresIn: process.env.JWT_TIMEOUT_DURATION,
					};
					const secret = process.env.JWT_SECRET;
					//Generated JWT token with Payload and secret.
					userData.token = jwt.sign(jwtPayload, secret, jwtData);

					return apiResponse.successResponseWithData(res,"Valid 2FA: Account confirmed", userData);
				}else{
					return apiResponse.unauthorizedResponse(res, "Invalid OTP: Otp does not match");
				}
			}else{
				return apiResponse.unauthorizedResponse(res, "Invalid OTP: Account is already valid.");
			}
		}else{
			return apiResponse.unauthorizedResponse(res, "Invalid OTP: Specified email not found. Register an account using this e-mail first.");
		}
	});
};


/**
 * Creates User
 *
 * @param req Request
 * @param res Response
 * @constructor
 */
exports.CreateUser = function(req, res) {

	// generate OTP for confirmation
	let otp = utility.generateRandomOTP(12);

	// Create User object with escaped and trimmed data
	var user = new UserModel(
		{
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			email: req.body.email,
			confirmOTP: otp,
			accessLevel: req.body.accessLevel
		}
	);
		// Html email body
	let html = mailer.buildEmail(String(otp));
	// Send confirmation email
	mailer.send(
		constants.confirmEmails.from,
		req.body.email,
		"Confirm Account",
		html
	).then(function(){
		// Save user.
		user.save(function (err) {
			if (err) { return apiResponse.ErrorResponse(res, err); }
			let userData = {
				_id: user._id,
				firstName: user.firstName,
				lastName: user.lastName,
				email: user.email,
				accessLevel: user.accessLevel
			};
			return apiResponse.successResponseWithData(res,"Registration Success.", userData);
		});
	}).catch(err => {
		console.log(err);
		return apiResponse.ErrorResponse(res,err);
	}) ;
};


/**
 * Login User Service
 * @param req Request
 * @param res Response callback
 * @returns response json
 * @constructor
 */
exports.AskTwoFactor = function(req, res) {
	let msgInfo = "Validation Error.";
	UserModel.findOne({email : req.body.email}).then(user => {
		if (user) {
			//Compare given password with db's hash.
			//Check account confirmation.
			if(user.isConfirmed){
				// Check User's account active or not.
				if(user.status) {
					let userData = {
						_id: user._id,
						firstName: user.firstName,
						lastName: user.lastName,
						email: user.email,
						accessLevel: user.accessLevel
					};


					const otp = utility.generateRandomOTP(8);

					UserModel.findOneAndUpdate({email : req.body.email}, {
						confirmOTP: otp
					}).catch(err => {
						return apiResponse.ErrorResponse(res, err);
					}).then( ()=>{

						// Html email body
						let html = mailer.buildEmail(String(otp));
						// Send confirmation email
						mailer.send(
							constants.confirmEmails.from,
							req.body.email,
							"2FA: Confirm you login",
							html
						).then(function(){
							return apiResponse.successResponse(res,"Check your email.");
						}).catch(err => {
							console.log(err);
							return apiResponse.ErrorResponse(res,err);
						}) ;
					});
			

				}else {
					msgInfo = "ERROR: Account is not active.";
					return apiResponse.unauthorizedResponse(res, msgInfo);
				}
			}else{
				msgInfo = "ERROR: Account is not confirmed. Missing OTP Validation.";
				return apiResponse.unauthorizedResponse(res, msgInfo);
			}

		}else{
			msgInfo = "ERROR: Email or Password wrong.";
			return apiResponse.unauthorizedResponse(res, msgInfo);
		}
	});
};

