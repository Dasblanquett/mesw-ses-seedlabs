// AUTHENTICATION CONTROLLER
// RESPONSIBLE FOR REGISTER, LOGIN AND OTP VALIDATION

// EXTERNAL
const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");

// INTERNAL
const apiResponse = require("../helpers/apiResponse");
const UserServices = require("../services/UserServices");
const auth = require("../middlewares/jwt");
const accessLevel = require("../middlewares/jwtAcessLevel");

/**
 * User registration.
 *
 * @returns {Object} response
 */
exports.register = [
	auth,
	// Validate fields.
	body("firstName").isLength({ min: 1 }).trim().withMessage("First name must be specified.")
		.isAlphanumeric().withMessage("First name has non-alphanumeric characters."),
	body("lastName").isLength({ min: 1 }).trim().withMessage("Last name must be specified.")
		.isAlphanumeric().withMessage("Last name has non-alphanumeric characters."),
	body("email").isLength({ min: 1 }).trim().withMessage("Email must be specified.")
		.isEmail().withMessage("Email must be a valid email address.").custom((value) => {
			return UserServices.CheckIfEmailExists(value);
		}),
	body("accessLevel").isLength({min:1, max:1}).trim().withMessage("Invalid Access Level.")
		.isInt({min:1, max:4}).withMessage("Invalid Access Level: Must be between 1 and 4."),
	// Sanitize fields.
	sanitizeBody("firstName").escape(),
	sanitizeBody("lastName").escape(),
	sanitizeBody("email").escape(),
	sanitizeBody("accessLevel").escape(),
	// Process request after validation and sanitization.
	(req, res) => {
		try {
			// Extract the validation errors from a request.
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				// Display sanitized values/errors messages.
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}else {

				if(!accessLevel.validateAccessLevelAndConfirmation(4,req)){
					return apiResponse.ErrorResponse(res, "Invalid access level.");
				}else{
					return UserServices.CreateUser(req, res);
				}
			}

		} catch (err) {
			//throw error in json response with status 500.
			return apiResponse.ErrorResponse(res, err);
		}
	}];

/**
 * User Login
 *
*/
exports.login = [
	body("email").isLength({ min: 1 }).trim().withMessage("Email must be specified.")
		.isEmail().withMessage("Email must be a valid email address."),
	body("password").isLength({ min: 1 }).trim().withMessage("Password must be specified."),
	sanitizeBody("email").escape(),
	sanitizeBody("password").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}else {
				return UserServices.UserLogin(req,res);
			}
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}];

/**
 * Confirm otp.
 * @returns {Object} response
 */
exports.verifyConfirm = [
	body("email").isLength({ min: 1 }).trim().withMessage("Email must be specified.")
		.isEmail().withMessage("Email must be a valid email address."),
	body("otp").isLength({ min: 1 }).trim().withMessage("OTP must be specified."),
	body("password").isLength({ min: 1 }).trim().withMessage("Password must be specified."),

	sanitizeBody("email").escape(),
	sanitizeBody("otp").escape(),
	sanitizeBody("password").escape(),

	(req, res) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}else {

				return UserServices.ConfirmOTP(req.body.email, req.body.otp, req.body.password, res);
			}
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}];

/**
 * Confirm otp.
 * @returns {Object} response
 */
exports.verifyTwoFactor = [
	auth,
	body("email").isLength({ min: 1 }).trim().withMessage("Email must be specified.")
		.isEmail().withMessage("Email must be a valid email address."),
	body("otp").isLength({ min: 1 }).trim().withMessage("OTP must be specified."),

	sanitizeBody("email").escape(),
	sanitizeBody("otp").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}else {

				return UserServices.VerifyTwoFactor(req.body.email, req.body.otp, res);
			}
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}];

/**
 * User Login
 *
 */
exports.askTwoFactor = [
	auth,
	body("email").isLength({ min: 1 }).trim().withMessage("Email must be specified.")
		.isEmail().withMessage("Email must be a valid email address."),
	sanitizeBody("email").escape(),
	(req, res) => {
		try {
			const errors = validationResult(req);
			if (!errors.isEmpty()) {
				return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
			}else {
				return UserServices.AskTwoFactor(req,res);
			}
		} catch (err) {
			return apiResponse.ErrorResponse(res, err);
		}
	}];