const { body,validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const accessLevel = require("../middlewares/jwtAcessLevel");

const MathServices = require("../services/MathServices");

exports.nRoot = [
	auth,
	body("rootIndex").isLength({min:1}).trim().withMessage("Invalid Root.")
		.isDecimal({min:0}).withMessage("Invalid Root Index"),
	body("rootParameter").isLength({min: 1}).trim().withMessage("Invalid Parameter.")
		.isDecimal({min:0}).withMessage("Invalid Root Parameter"),
	// Sanitize fields.
	sanitizeBody("rootIndex").escape(),
	sanitizeBody("rootParameter").escape(),
	function (req, res) {

		if(!accessLevel.validateAccessLevelAndConfirmation(3,req)){
			return apiResponse.ErrorResponse(res, "Invalid access level.");
		}

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			// Display sanitized values/errors messages.
			return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
		}

		try{
			return apiResponse.successResponseWithData(res,
				"Operation Success",
				MathServices.NRoot(parseFloat(req.body.rootIndex), parseFloat(req.body.rootParameter)));
		}catch (err){
			return apiResponse.ErrorResponse(res, "Validation Error.", errors.array());
		}

	}
];

exports.cubicRoot = [
	auth,
	body("rootParameter").isLength({min: 1}).trim().withMessage("Invalid Parameter.")
		.isDecimal({min:0}).withMessage("Invalid Root Parameter"),
	// Sanitize fields.
	sanitizeBody("rootParameter").escape(),
	function (req, res) {

		if(!accessLevel.validateAccessLevelAndConfirmation(2,req)){
			return apiResponse.ErrorResponse(res, "Invalid access level.");
		}

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			// Display sanitized values/errors messages.
			return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
		}

		try{
			return apiResponse.successResponseWithData(res,
				"Operation Success",
				MathServices.NRoot(3, parseFloat(req.body.rootParameter)));
		}catch (err){
			return apiResponse.ErrorResponse(res, "Validation Error.", errors.array());
		}

	}
];

exports.squareRoot = [
	auth,
	body("rootParameter").isLength({min: 1}).trim().withMessage("Invalid Parameter.")
		.isDecimal({min:0}).withMessage("Invalid Root Parameter"),
	// Sanitize fields.
	sanitizeBody("rootParameter").escape(),
	function (req, res) {

		if(!accessLevel.validateAccessLevelAndConfirmation(1,req)){
			return apiResponse.ErrorResponse(res, "Invalid access level.");
		}

		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			// Display sanitized values/errors messages.
			return apiResponse.validationErrorWithData(res, "Validation Error.", errors.array());
		}

		try{
			return apiResponse.successResponseWithData(res,
				"Operation Success",
				MathServices.NRoot(2, parseFloat(req.body.rootParameter)));
		}catch (err){
			return apiResponse.ErrorResponse(res, "Validation Error.", errors.array());
		}

	}
];
