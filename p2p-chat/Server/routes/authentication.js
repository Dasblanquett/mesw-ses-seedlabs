var express = require("express");
const AuthenticationController = require("../controllers/AuthenticationController");

var router = express.Router();

// GET

// POST
router.post("/register", AuthenticationController.register);
router.post("/login", AuthenticationController.login);
router.post("/verify-otp", AuthenticationController.verifyConfirm);
router.post("/verify-2fa", AuthenticationController.verifyTwoFactor);
router.post("/ask-2fa", AuthenticationController.askTwoFactor);



// PUT

// DELETE

module.exports = router;