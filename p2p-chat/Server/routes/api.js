var express = require("express");
var authenticationRouter = require("./authentication");
var mathRouter = require("./mathServices");


var app = express();

// api routes
app.use("/auth/", authenticationRouter);
app.use("/math/", mathRouter);

module.exports = app;