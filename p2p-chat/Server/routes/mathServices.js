var express = require("express");
const MathServicesController = require("../controllers/MathServicesController");

var router = express.Router();

// square root, cubic root, and a parameterized n-root,
// GET
// /math
router.post("/2root", MathServicesController.squareRoot); // CL1
router.post("/3root", MathServicesController.cubicRoot); // CL2
router.post("/nroot", MathServicesController.nRoot); // CL3

// POST

// PUT

// DELETE

module.exports = router;