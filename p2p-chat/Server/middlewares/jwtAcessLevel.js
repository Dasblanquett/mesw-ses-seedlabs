var jwt = require("jsonwebtoken");
const secret = process.env.JWT_SECRET;

/**
 * Token based validation for Access Level and Confimed accounts
 *
 * @param accessLevel The API accessLevel
 * @param req The HTTP request
 * @returns {boolean}
 */
exports.validateAccessLevelAndConfirmation = function(accessLevel, req) {

	const usertoken = req.headers.authorization;
	const token = usertoken.split(" ");
	const decoded = jwt.verify(token[1], secret);
	console.log(decoded);
	return decoded.accessLevel >= accessLevel && decoded.twoFactor == true;
};