/**
 * Hack to generate random Alphanumeric based on https://stackoverflow.com/a/10727155
 *
 * @param length :: The size of the OTP
 * @returns {string} :: The Generated OTP
 */
exports.generateRandomOTP = function (length) {
	var text = "";
	var possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (var i = 0; i < length; i++) {
		var sup = Math.floor(Math.random() * possible.length);
		text += i > 0 && sup == i ? "0" : possible.charAt(sup);
	}
	return text;
};