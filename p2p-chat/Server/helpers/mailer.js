// FROM https://nodemailer.com/

const nodemailer = require("nodemailer");

/**
 * Send Email
 *
 * @param from sender address
 * @param to list of receivers
 * @param subject Subjet
 * @param html email body
 *
 * @returns {*|Promise<any>} result
 */
exports.send = function (from, to, subject, html)
{
	// create new SMTP transport
	let transporter = nodemailer.createTransport({
		service: "Gmail",
		auth: {
			user: process.env.EMAIL_SMTP_USERNAME,
			pass: process.env.EMAIL_SMTP_PASSWORD
		}
	});

	// send email
	return transporter.sendMail({
		from: from, // sender address
		to: to, // list of receivers
		subject: subject, // Subjet
		html: html // body
	});
};

/**
 * Builds an email based on OTP
 *
 * @param otp The OTP to use
 * @returns {string} The body of the email (html)
 */
exports.buildEmail = function (otp) {
	return "<p>Please Confirm your Account.</p><p>OTP: "+otp+"</p>";
};