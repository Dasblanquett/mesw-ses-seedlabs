import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../services/register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-confirm-otp',
  templateUrl: './confirm-otp.component.html',
  styleUrls: ['./confirm-otp.component.css']
})
export class ConfirmOtpComponent implements OnInit {

  email: string;
  password: string;
  otp: string;

  constructor(
    private registerService: RegisterService,
    private route: Router,
  ) { }

  ngOnInit() {
  }

  confirmaccount(): void {
    this.registerService.confirm_otp(this.email, this.password, this.otp).subscribe(res => {
      if (res.status === 1) {
        window.alert('Confirmed Account');
        this.route.navigate(['login']);
      } else {window.alert('Erro'); }
    });


  }

}
