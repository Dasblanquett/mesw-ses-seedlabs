import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RootserviceService} from "../services/rootservice.service";

@Component({
  selector: 'app-parameterroot',
  templateUrl: './parameterroot.component.html',
  styleUrls: ['./parameterroot.component.css']
})
export class ParameterrootComponent implements OnInit {

  private result: number;

  constructor(
    private http: HttpClient,
    private Rootservice: RootserviceService
  ) { }

  ngOnInit() {
  }

  calculateN(parametro: number, indice: number): void {

    if (!parametro) {return; }

    this.Rootservice.calculateN(parametro, indice)
      .subscribe(res => this.result = res.data.result);
  }

}
