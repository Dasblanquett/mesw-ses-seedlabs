import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterrootComponent } from './parameterroot.component';

describe('ParameterrootComponent', () => {
  let component: ParameterrootComponent;
  let fixture: ComponentFixture<ParameterrootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParameterrootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterrootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
