export class backendSettings {

    deploy: string = "https://quiet-shore-81414.herokuapp.com";
    localhost: string = "http://localhost:3000";
    https_localhost: string = "https://localhost:3001"

}
