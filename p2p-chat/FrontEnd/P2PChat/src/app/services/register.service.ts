import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import {RegisterDto} from '../model/registerdto';
import {backendSettings} from './backendSettings';
import {UserLogin} from "../model/userLogin";
import {confirmDto} from "../model/confirmDto";


@Injectable({
    providedIn: 'root'
})
export class RegisterService {
    backend : backendSettings = new backendSettings();

    private registerUrl = `${this.backend.https_localhost}/api/auth/register`;
    private verifyUrl = `${this.backend.https_localhost}/api/auth/verify-otp`;

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient) { }

    /** POST: add a new User to the server */
    register(register: RegisterDto): Observable<RegisterDto> {

        console.log(this.registerUrl);

        console.log(register);

        return this.http.post<RegisterDto>(this.registerUrl, register, this.httpOptions)
            .pipe(
                catchError(this.handleError<RegisterDto>('register'))
            );
    }

    confirm_otp(email: string, password: string, otp: string){
      let dto: confirmDto = new confirmDto();
      dto.email = email;
      dto.password = password;
      dto.otp = otp;

      return this.http.post<any>(this.verifyUrl, dto).pipe(
        catchError(this.handleError<string>('Verify')));
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}
