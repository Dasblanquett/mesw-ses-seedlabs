import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {RegisterDto} from "../model/registerdto";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TokenParserService {

constructor() { }

  decodeToEmail(myRawToken): string {

    const helper = new JwtHelperService();

    const decodedToken = helper.decodeToken(myRawToken);

    return decodedToken.email;
  }
}
