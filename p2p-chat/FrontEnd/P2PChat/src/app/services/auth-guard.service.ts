import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private _router:Router, private _snackBar: MatSnackBar ) {
  }



  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    const expectedRoles = route.data.data.role;
    console.log(expectedRoles);
    console.log(localStorage.getItem("ROLE"))

    if ( expectedRoles.find(element => element == localStorage.getItem("ROLE")) === undefined || // Role isn't expected
     localStorage.getItem("isLoggedIn") == 'false' ||                                          // Not logged in
     localStorage.getItem("token") == undefined)  {                                            // Token not defined
    

      this._snackBar.open('You are not allowed to view this page - Login First', "OK");

      
      this._router.navigate(["login"]);
      return false;

    }
    
    return true;
  }

}
