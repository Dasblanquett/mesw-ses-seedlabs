import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {backendSettings} from './backendSettings';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {RootDto} from "../model/rootDto";

@Injectable({
  providedIn: 'root'
})
export class RootserviceService {

  backend: backendSettings = new backendSettings();
  tokenParse = localStorage.getItem('token');

  private squareRootPath =  `${this.backend.https_localhost}/api/math/2root`;
  private cubicRootPath  =  `${this.backend.https_localhost}/api/math/3root`;
  private nRootPath =  `${this.backend.https_localhost}/api/math/nroot`;



  httpOptionsToken = {
    headers: new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .append('Authorization', `Bearer ${this.tokenParse}`)
      .append('Access-Control-Allow-Origin', '*')
      .append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
      .append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  };

  constructor(
    private http: HttpClient
  ) { }

  calculateSquare(parametro: number): Observable<any> {

    let dto = new RootDto();
    dto.rootIndex = 2;
    dto.rootParameter = parametro;

    console.log("param:", JSON.stringify(dto));

    return this.http.post<any>(this.squareRootPath, dto
      , this.httpOptionsToken).pipe(
      catchError(this.handleError<string>('Square Root'))
    );
  }

  calculateCubic(parametro: number): Observable<any> {

    let dto = new RootDto();
    dto.rootIndex = 3;
    dto.rootParameter = parametro;

    console.log("param:", JSON.stringify(dto));

    return this.http.post<any>(this.cubicRootPath, dto
      , this.httpOptionsToken).pipe(
      catchError(this.handleError<string>('Cubic Root'))
    );
  }

  calculateN(parametro: number, indice: number): Observable<any> {

    let dto = new RootDto();
    dto.rootIndex = indice;
    dto.rootParameter = parametro;

    console.log("param:", JSON.stringify(dto));

    return this.http.post<any>(this.nRootPath, dto
      , this.httpOptionsToken).pipe(
      catchError(this.handleError<string>('N Root'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
