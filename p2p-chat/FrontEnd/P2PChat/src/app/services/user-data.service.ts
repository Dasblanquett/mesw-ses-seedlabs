import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { backendSettings } from './backendSettings';
import { UserLogin } from '../model/userLogin';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  backend: backendSettings = new backendSettings();
  tokenParse = localStorage.getItem("token");

  private twoFactorPath = `${this.backend.https_localhost}/api/auth/verify-2fa`;
  private retryTwoFactorPath = `${this.backend.https_localhost}/api/auth/ask-2fa`;

  httpOptionsToken = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }).append('Authorization', `Bearer ${this.tokenParse}`)
  };


  private loginPath = `${this.backend.https_localhost}/api/auth/login`;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }


  login(email: String, password: String): Observable<any> {

    let dto: UserLogin = new UserLogin();
    dto.email = email;
    dto.password = password;


    return this.http.post<any>(this.loginPath, dto).pipe(
      catchError(this.handleError<string>('Login'))
    );

  }

  verify2fa(email: String, otp: String): Observable<any> {

    return this.http.post<any>(this.twoFactorPath, {
      otp: otp,
      email: email
    }, this.httpOptionsToken).pipe(
      catchError(this.handleError<string>('Verify 2FA'))
    )

  }

  ask2fa(email: String, otp: String): Observable<any> {

    return this.http.post<any>(this.retryTwoFactorPath, {
      otp: otp,
      email: email
    }, this.httpOptionsToken).pipe(
      catchError(this.handleError<string>('Verify 2FA'))
    )

  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
