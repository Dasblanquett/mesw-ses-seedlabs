import { TestBed } from '@angular/core/testing';

import { TokenParserService } from './token-parser.service';

describe('TokenParserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TokenParserService = TestBed.get(TokenParserService);
    expect(service).toBeTruthy();
  });
});
