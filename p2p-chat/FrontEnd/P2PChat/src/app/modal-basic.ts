import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TestBed } from '@angular/core/testing';

@Component({
  selector: 'ngbd-modal-basic',
  templateUrl: './modal-basic.html'
})
export class NgbdModalBasic {
  warningText: string;
  warningTypeList = [
    {
      text: 'Register a pet before searching for a sitter!',
      closePath: 'mypets'
    },
    {
      text: 'Resquest sent successfully!',
      closePath: 'mypets'
    },
    {
      text: 'A problem was found while submitting the request...',
      closePath: 'findpetsitters'
    }
  ]
  warningType;
  constructor(
    private modalService: NgbModal,
    private router: Router
  ) { }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title', centered: true }).result.then((result) => {
      this.nextScreen();
    }, (reason) => {
      this.nextScreen();
    });
  }

  simulateClick(id) {

    this.warningType = this.warningTypeList[id];


    this.warningText = this.warningType.text;
    document.getElementById("openModalButton").click();
  }

  nextScreen() {
    //this.router.navigate([this.warningType.closePath]);
  }
}
