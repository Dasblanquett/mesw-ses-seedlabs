import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { transformAll } from '@angular/compiler/src/render3/r3_ast';

import { Location } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  flag :boolean = false;
  userImage;
  hasImage = true;
  activeTab = 'home';
  isPetOwner;
  public isMenuCollapsed = true;
  public showLink = Boolean(localStorage.getItem("isLoggedIn"));

  constructor(
    private router: Router,
    private location: Location) {

    router.events.subscribe(val => {
      if (location.path() != "") {

        if(this.flag === false){
          this.ngOnInit();
          this.showLink = Boolean(localStorage.getItem("isLoggedIn"));
        }
      }
    });
  }

  ngOnInit() {
    if (Boolean(localStorage.getItem("isLoggedIn"))) {
    }
  }


  brand() {
    if (Boolean(localStorage.getItem("isLoggedIn"))) {
      this.router.navigate(["home"]);
      this.activeTab = 'home';
    } else if(Boolean(localStorage.getItem("isLoggedIn")) && localStorage.getItem("ROLE") === "3"){
      this.router.navigate(["admub"]);
      this.activeTab = 'sittingHistory';
    }else {
      this.router.navigate([""]);
    }
  }

  logOut(activeTab) {
    this.activeTab = activeTab;
    this.isMenuCollapsed = true
    localStorage.clear();
    this.router.navigate([""]);
    this.showLink = Boolean(localStorage.getItem("isLoggedIn"));
    location.reload();
  }
}
