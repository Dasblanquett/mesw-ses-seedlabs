import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RootserviceService} from '../services/rootservice.service';

@Component({
  selector: 'app-squareroot',
  templateUrl: './squareroot.component.html',
  styleUrls: ['./squareroot.component.css']
})
export class SquarerootComponent implements OnInit {

  private result: number;

  constructor(
    private http: HttpClient,
    private Rootservice: RootserviceService
  ) { }

  ngOnInit() {
  }

  calculateSquare(parametro: number): void {

    if (!parametro) {return; }

    this.Rootservice.calculateSquare(parametro)
      .subscribe(res => this.result = res.data.result);
  }

}
