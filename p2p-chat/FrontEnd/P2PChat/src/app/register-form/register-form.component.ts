import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import {Router} from '@angular/router';

import { ConfirmEqualValidatorDirective } from '../validators/confirm-equal-validator.directive';

import { RegisterService } from '../services/register.service';
import { RegisterDto } from '../model/registerdto';
import {first} from 'rxjs/operators';


declare var NgForm: any;

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {


  registerForm: FormGroup;
  submitted = false;
  loading = false;
  error: string;

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private registerService: RegisterService,
    private router: Router) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      lastName: ['', Validators.required],
      accessLevel: ['', Validators.required]
    });

  }

  // conveniencia para acesso mais rápido ao frontend.
  get fval() {
    return this.registerForm.controls;
  }

  signup() {

    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;

    const register = new RegisterDto(
      this.registerForm.value.firstName,
      this.registerForm.value.email,
      this.registerForm.value.lastName,
      this.registerForm.value.accessLevel
    );

    if (!this.registerForm.invalid) {
      this.registerService.register(register).subscribe(()=>{this.router.navigate(['home']); });
    }

  }

}
