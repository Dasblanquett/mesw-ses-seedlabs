import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubicrootComponent } from './cubicroot.component';

describe('CubicrootComponent', () => {
  let component: CubicrootComponent;
  let fixture: ComponentFixture<CubicrootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubicrootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubicrootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
