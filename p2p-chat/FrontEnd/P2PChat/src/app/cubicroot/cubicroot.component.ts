import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RootserviceService} from "../services/rootservice.service";

@Component({
  selector: 'app-cubicroot',
  templateUrl: './cubicroot.component.html',
  styleUrls: ['./cubicroot.component.css']
})
export class CubicrootComponent implements OnInit {

  private result: number;

  constructor(
    private http: HttpClient,
    private Rootservice: RootserviceService
  ) { }

  ngOnInit() {
  }

  calculateCubic(parametro: number): void {

    if (!parametro) {return; }

    this.Rootservice.calculateCubic(parametro)
      .subscribe(res => this.result = res.data.result);
  }
}
