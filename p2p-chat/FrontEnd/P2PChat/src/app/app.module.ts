import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { HttpClientModule }    from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './auth/token.interceptor';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { LoginComponent } from './login/login.component';


import { LoginGuard } from './auth/guards/LoginGuard';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ErrorComponent } from './error/error.component';
import { SuccessComponent } from './success/success.component';

import { RegisterFormComponent } from './register-form/register-form.component';
import { ConfirmEqualValidatorDirective } from './validators/confirm-equal-validator.directive';
import { ReactiveFormsModule }   from '@angular/forms';

import { NgbdModalBasic } from './modal-basic';
import { TwoFactorComponent } from './two-factor/two-factor.component';
import { HomeComponent } from './home/home.component';
import { SquarerootComponent } from './squareroot/squareroot.component';
import { CubicrootComponent } from './cubicroot/cubicroot.component';
import { ParameterrootComponent } from './parameterroot/parameterroot.component';
import { ConfirmOtpComponent } from './confirm-otp/confirm-otp.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,

    ErrorComponent,
    SuccessComponent,
    RegisterFormComponent,
    ConfirmEqualValidatorDirective,

    NgbdModalBasic,

    TwoFactorComponent,

    HomeComponent,

    SquarerootComponent,

    CubicrootComponent,

    ParameterrootComponent,

    ConfirmOtpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  model;
}
