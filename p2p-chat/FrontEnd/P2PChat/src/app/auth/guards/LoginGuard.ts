import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor() {}

    canActivate() {
        alert("Can activiate")
      return localStorage.getItem("role") == undefined;
    }

}