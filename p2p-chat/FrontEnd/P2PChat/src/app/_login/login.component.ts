import { Component, OnInit, Input } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model = undefined;

  email: string;
  password: string;
  info: string;

  invalidLogin: boolean;
  showBox: boolean;
  loading = false;

  constructor(private loginSerivce: UserDataService, private router: Router
  ) { }

  ngOnInit() {

    this.showBox = false;

  }

  login(): void {
    this.loginSerivce.login(this.email, this.password).subscribe(res => {
      this.loading = true;

      this.invalidLogin = false;

      this.showBox = true;

      if (res != null) {
        // localStorage.setItem("email", this.email);
        // localStorage.setItem("id", res.data._id);
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("isLoggedIn", 'true');

      //  localStorage.setItem("ROLE", res.data.accessLevel);
      //  localStorage.setItem("ROLE", res.data.accessLevel);
            this.router.navigate(["home"]);

      } else {
        this.loading = false;
        localStorage.setItem("isLoggedIn", 'false');
        this.router.navigate(["Error"]);

      }
    }, err => {

      console.log("Tentativa FALHADA Loggin Cliente USERNAME/EMAIL<" + this.email + ">");

      alert("Acesso não autorizado:");
      this.invalidLogin = true;
      this.showBox = false;
    })

  }


  auth(): void {

    alert(this.info);
    localStorage.setItem("username", this.email);
    localStorage.setItem("jwt", this.info);

    console.log(" Loggin  USERNAME/EMAIL<" + this.email + ">");

  }



}
