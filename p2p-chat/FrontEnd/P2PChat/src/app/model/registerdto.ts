export class RegisterDto {
  firstName: string;
  email: string;
  lastName: string;
  accessLevel: string;

  constructor(firstName: string, email: string, lastName: string, accessLevel: string) {
    this.firstName = firstName;
    this.email = email;
    this.lastName = lastName;
    this.accessLevel = accessLevel;
  }
}
