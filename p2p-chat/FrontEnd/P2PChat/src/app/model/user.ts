export class User {
    username: string;
    password: string;
    email: string;
    name: string;
    mobile: number;

    constructor(username: string, password:string, email: string, name: string, mobile: number) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.mobile = mobile;
    }
}