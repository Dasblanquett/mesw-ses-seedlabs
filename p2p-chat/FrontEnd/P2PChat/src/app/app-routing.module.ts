import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SuccessComponent } from './success/success.component';
import { ErrorComponent } from './error/error.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import {TwoFactorComponent} from "./two-factor/two-factor.component";
import {HomeComponent} from "./home/home.component";
import {SquarerootComponent} from "./squareroot/squareroot.component";
import {CubicrootComponent} from "./cubicroot/cubicroot.component";
import {ParameterrootComponent} from "./parameterroot/parameterroot.component";
import {ConfirmOtpComponent} from "./confirm-otp/confirm-otp.component";

const routes: Routes = [
  { path: 'register', component: RegisterFormComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: '2fa', component: TwoFactorComponent },
  { path: 'home', component: HomeComponent },
  { path: 'squareroot', component: SquarerootComponent },
  { path: 'cubicroot', component: CubicrootComponent},
  { path: 'parameterroot', component: ParameterrootComponent},
  { path: 'confirm_otp', component: ConfirmOtpComponent},


  /*
  { path: 'mypets', component: MyPetsDashboardComponent, canActivate: [AuthGuardService], data: {
    data: {
      role: ['ADMIN', 'PETOWNER']
    }
  } },
*/

  { path: 'Success', component : SuccessComponent },
  { path: 'Error', component : ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] })
export class AppRoutingModule { }
