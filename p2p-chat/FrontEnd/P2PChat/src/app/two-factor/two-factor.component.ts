import { Component, OnInit, Input } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'two-factor',
  templateUrl: './two-factor.component.html',
  styleUrls: ['./two-factor.component.css']
})
export class TwoFactorComponent implements OnInit {

  model = undefined;

  email: string = localStorage.getItem("email");
  otp: string;
  info: string;

  invalidLogin: boolean;
  showBox: boolean;
  loading = false;

  constructor(private loginSerivce: UserDataService, private router: Router
  ) { }

  ngOnInit() {

    this.showBox = false;

  }

  login(): void {
    this.loginSerivce.verify2fa(this.email, this.otp).subscribe(res => {
      this.loading = true;

      this.showBox = true;

      if (res != null) {
        localStorage.setItem("twoFactor", "true");
        localStorage.setItem("token", res.data.token);

        this.router.navigate(["home"]);

      } else {
        this.loading = false;
        localStorage.setItem("isLoggedIn", 'false');
        this.router.navigate(["Error"]);

      }

    }, err => {

      console.log("Tentativa FALHADA Loggin Cliente USERNAME/EMAIL<" + this.email + ">");

      alert("Acesso não autorizado:");
      this.invalidLogin = true;
      this.showBox = false;
    })

  }


}
