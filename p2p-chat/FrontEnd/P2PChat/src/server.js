//Install express server
const express = require('express');
const path = require('path');
var cors = require('cors')

const nomeApp = process.env.npm_package_name;
const app = express();

app.use(cors())

// Serve only the static files form the dist directory
app.use(express.static(process.cwd() + '/dist/pet-management-feature'));

app.get('/*', function(req,res) {

req.header("Access-Control-Allow-Origin", "*");
req.header("Access-Control-Allow-Credentials", true);
req.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
req.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");


res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Credentials", true);
res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");

res.sendFile(path.join(process.cwd() + '/dist/pet-management-feature/index.html'));
});







// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
console.log("Angular PetCare is listening... A");
